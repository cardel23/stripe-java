package com.smbs.stripeTest.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.stripe.model.StripeObject;

public class StripeObjectSerializer extends JsonSerializer<StripeObject> {

    @Override
    public void serialize(StripeObject stripeObject, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
//        stripeObject.toJson();
//        Gson gson = new Gson();
//        gson.fromJson(stripeObject.toJson(), StripeObject.class);
//        JsonParser parser = new JsonParser();
//        
//        JsonObject object = (JsonObject) parser.parse(stripeObject.toJson());
//        object.entrySet();
//        HashMap<String, Object> params = new HashMap<String, Object>();
//        params.put("paymentMethod", buildObject(object));
        
    }
    
    private HashMap<String, Object> buildObject (JsonElement element) {
    	JsonObject object = element.getAsJsonObject();
    	HashMap<String, Object> params = new HashMap<String, Object>();
    	Set<Map.Entry<String, JsonElement>> entries = object.entrySet();//will return members of your object
    	for (Map.Entry<String, JsonElement> entry: entries) {
    		if(!entry.getKey().equals("rawJsonObject")) {
    			if(entry.getValue().isJsonObject())
    				params.put(entry.getKey(), buildObject(entry.getValue()));
    			else params.put(entry.getKey(), entry.getValue());
    		}
    	    
    	}
    	return params;
    }
    
}