package com.smbs.stripeTest.controller;

import com.smbs.stripeTest.http.PaymentIntentDto;
import com.smbs.stripeTest.http.SubscriptionDto;
import com.smbs.stripeTest.service.PaymentService;
import com.smbs.stripeTest.service.SubscriptionService;
import com.smbs.stripeTest.service.WebhookService;
import com.smbs.stripeTest.utils.Examples;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.model.Refund;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiResponse;
//import io.swagger.annotations.ApiResponses;
//import io.swagger.annotations.Example;
//import io.swagger.annotations.ExampleProperty;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/stripe")
@CrossOrigin(origins = "*")
public class StripeController {

    @Autowired
    PaymentService paymentService;
    @Autowired
    SubscriptionService subscriptionService;
    @Autowired
    WebhookService webhookService;
	HttpHeaders headers = new HttpHeaders();

    
    public StripeController() {
	    headers.setContentType(MediaType.APPLICATION_JSON);
	}

    @ResponseBody
    @PostMapping("/paymentIntent")
    @Operation(summary = "Create a payment intent")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
            description = "Success",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.CREATE_PAYMENT_RESPONSE)})}),
            @ApiResponse(responseCode = "500",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR500)})}),
            @ApiResponse(responseCode = "404",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR404)})})})
    public ResponseEntity<String> payment(@RequestBody PaymentIntentDto paymentIntentDto) throws StripeException {
        PaymentIntent paymentIntent = paymentService.paymentIntent(paymentIntentDto);
        String paymentStr = paymentIntent.toJson();
        return new ResponseEntity<String>(paymentStr, headers, HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/confirmPayment/{id}")
    @Operation(summary = "Confirm a payment intent")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
            description = "Success",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.GET_PAYMENT_RESPONSE)})}),
            @ApiResponse(responseCode = "500",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR500)})}),
            @ApiResponse(responseCode = "404",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR404)})})})
    public ResponseEntity<String> confirmPayment(@PathVariable("id") String id) throws StripeException {
        PaymentIntent paymentIntent = paymentService.confirm(id);
        String paymentStr = paymentIntent.toJson();
        return new ResponseEntity<String>(paymentStr, headers, HttpStatus.OK);
    }
    
    @ResponseBody
    @PostMapping("/cancelPayment/{id}")
    @Operation(summary = "Cancel a payment intent")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
            description = "Success",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.CANCEL_PAYMENT_RESPONSE)})}),
            @ApiResponse(responseCode = "500",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR500)})}),
            @ApiResponse(responseCode = "404",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR404)})})})
    public ResponseEntity<String> cancelPayment(@PathVariable("id") String id) throws StripeException {
        PaymentIntent paymentIntent = paymentService.cancel(id);
        String paymentStr = paymentIntent.toJson();
        return new ResponseEntity<String>(paymentStr, headers, HttpStatus.OK);
    }
    
    @ResponseBody
    @GetMapping("/paymentIntent/{id}")
    @Operation(summary = "Get a payment intent")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
            description = "Success",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.CREATE_PAYMENT_RESPONSE)})}),
            @ApiResponse(responseCode = "500",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR500)})}),
            @ApiResponse(responseCode = "404",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR404)})})})
    public ResponseEntity<String> getPaymentIntent(@PathVariable("id") String id) throws StripeException {
    	PaymentIntent paymentIntent = paymentService.retrieve(id);
    	String paymentStr = paymentIntent.toJson();
        return new ResponseEntity<String>(paymentStr, headers, HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/revertCharge/{id}")
    @Operation(summary = "Revert payment")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
            description = "Success",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.REVERT_CHARGE_RESPONSE)})}),
            @ApiResponse(responseCode = "500",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR500)})}),
            @ApiResponse(responseCode = "404",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR404)})})})
    public ResponseEntity<String> cancel(@PathVariable("id") String id) throws StripeException {
    	Refund refund = paymentService.refund(id);
        String refundStr = refund.toJson();
        return new ResponseEntity<String>(refundStr, headers, HttpStatus.OK);
    }
    
    @ResponseBody
    @PostMapping("/subscription")
    @Operation(summary = "Create a subscription")
    @Parameters({@Parameter(name = "paymentMethod", ref = "PaymentMethodDto")})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
            description = "Success",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.CREATE_SUBSCRIPTION_RESPONSE)})}),
            @ApiResponse(responseCode = "500",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR500)})}),
            @ApiResponse(responseCode = "404",
            description = "Error",
	            content = {@Content(mediaType = "application/json",
	            examples = {@ExampleObject(Examples.ERROR404)})})})
    public ResponseEntity<String> createSubscription(@RequestBody SubscriptionDto subscriptionDto) throws StripeException {
	    String subscriptionStr = subscriptionService.subscribe(subscriptionDto);
	    return new ResponseEntity<String>(subscriptionStr, headers, HttpStatus.OK);
    }
    
    @ResponseBody
    @PostMapping("/webhook")
    public ResponseEntity<String> postEventsWebhook(HttpServletRequest request, HttpServletResponse response) throws StripeException  {
        String result = webhookService.getEvents(request, response);
        return new ResponseEntity<String>(result, headers, HttpStatus.OK);
    }
    
}
