package com.smbs.stripeTest.service;

import com.smbs.stripeTest.http.PaymentIntentDto;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.model.Refund;
import com.stripe.param.RefundCreateParams;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PaymentService {

    @Value("${stripe.key.secret}")
    String secretKey;
    
    public PaymentService() {
		// TODO Auto-generated constructor stub
        Stripe.apiKey = secretKey;
	}

    public PaymentIntent paymentIntent(PaymentIntentDto paymentIntentDto) throws StripeException {
        Stripe.apiKey = secretKey;
        List<String> paymentMethodTypes = new ArrayList();
        paymentMethodTypes.add("card");
        Map<String, Object> params = new HashMap<>();
        params.put("amount", paymentIntentDto.getAmount());
        params.put("currency", paymentIntentDto.getCurrency());
        params.put("description", paymentIntentDto.getDescription());
        params.put("payment_method", paymentIntentDto.getToken());
        params.put("payment_method_types", paymentMethodTypes);
        return PaymentIntent.create(params);
    }

    public PaymentIntent confirm(String id) throws StripeException {
        Stripe.apiKey = secretKey;
        PaymentIntent paymentIntent = retrieve(id);
        paymentIntent.confirm();
        return paymentIntent;
    }

    public PaymentIntent cancel(String id) throws StripeException {
        Stripe.apiKey = secretKey;
        PaymentIntent paymentIntent = retrieve(id);
        paymentIntent.cancel();
        return paymentIntent;
    }
    
    public PaymentIntent retrieve(String id) throws StripeException {
        Stripe.apiKey = secretKey;
    	PaymentIntent paymentIntent = PaymentIntent.retrieve(id);
    	return paymentIntent;
    }
    
    public Refund refund (String chargeId) throws StripeException {
        Stripe.apiKey = secretKey;
    	RefundCreateParams params = RefundCreateParams.builder().
    			setCharge(chargeId)
    			.build();
    	Refund refund = Refund.create(params);
    	return refund;
    }
}
