package com.smbs.stripeTest.service;

import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.stripe.Stripe;
import com.stripe.exception.SignatureVerificationException;
import com.stripe.model.Charge;
import com.stripe.model.Event;
import com.stripe.model.EventDataObjectDeserializer;
import com.stripe.model.Invoice;
import com.stripe.model.PaymentIntent;
import com.stripe.model.StripeObject;
import com.stripe.model.Subscription;
import com.stripe.net.Webhook;
import com.stripe.param.SubscriptionUpdateParams;

@Service
public class WebhookService {

	@Value("${stripe.key.secret}")
	String secretKey;

	String stripeWebhookSecret = "whsec_L3W4eh11G3KmY8h4DyDkxZPVsJA31twt";

	public String getEvents(HttpServletRequest request, HttpServletResponse response) {
		Stripe.apiKey = secretKey;
		String result = "";
		try {
			String payload = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

			if (!StringUtils.isEmpty(payload)) {
				String sigHeader = request.getHeader("Stripe-Signature");
				String endpointSecret = stripeWebhookSecret;

				Event event;
				try {
					event = Webhook.constructEvent(payload, sigHeader, endpointSecret);
				} catch (SignatureVerificationException e) {
					// Invalid signature
					response.setStatus(400);
					return "";
				}

				// Deserialize the nested object inside the event
				EventDataObjectDeserializer dataObjectDeserializer = event.getDataObjectDeserializer();
				StripeObject stripeObject = null;
				if (dataObjectDeserializer.getObject().isPresent()) {
					stripeObject = dataObjectDeserializer.getObject().get();
				} else {
					// Deserialization failed, probably due to an API version mismatch.
					// Refer to the Javadoc documentation on `EventDataObjectDeserializer` for
					// instructions on how to handle this case, or return an error here.
				}
				switch (event.getType()) {
					case "invoice.paid":
						// Used to provision services after the trial has ended.
						// The status of the invoice will show up as paid. Store the status in your
						// database to reference when a user accesses your service to avoid hitting rate
						// limits.
						Invoice invoice = (Invoice) stripeObject;
						if(invoice.getBillingReason().equals("subscription_create")) {
						  String subscriptionId = invoice.getSubscription();
						  String paymentIntentId = invoice.getPaymentIntent();
	
						  // Retrieve the payment intent used to pay the subscription
						  PaymentIntent paymentIntent = PaymentIntent.retrieve(paymentIntentId);
	
						  // Set the default payment method
						  Subscription subscription = Subscription.retrieve(subscriptionId);
						  SubscriptionUpdateParams params = SubscriptionUpdateParams
						    .builder()
						    .setDefaultPaymentMethod(paymentIntent.getPaymentMethod())
						    .build();
						  Subscription updatedSubscription = subscription.update(params);
						  result = updatedSubscription.toJson();
						}
						System.out.println(event.getType());
						break;
					case "invoice.payment_failed":
						// If the payment fails or the customer does not have a valid payment method,
						// an invoice.payment_failed event is sent, the subscription becomes past_due.
						// Use this webhook to notify your user that their payment has
						// failed and to retrieve new card details.
						System.out.println(event.getType());
						break;
					case "customer.subscription.deleted":
						// handle subscription cancelled automatically based
						// upon your subscription settings. Or if the user
						// cancels it.
						System.out.println(event.getType());
						break;
					case "charge.refunded":
						Gson gson = new Gson();
						Charge charge = gson.fromJson(event.getData().getObject().toJson(), Charge.class);
						result = charge.toJson();
					default:
						// Unhandled event type
						System.out.println(event.getType());
					}

				response.setStatus(200);
			}

		} catch (Exception e) {
			response.setStatus(500);
		}
		return result;
	}

}
