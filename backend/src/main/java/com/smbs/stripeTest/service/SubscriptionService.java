package com.smbs.stripeTest.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.smbs.stripeTest.http.SubscriptionDto;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Customer;
import com.stripe.model.PaymentMethod;
import com.stripe.model.StripeObject;
import com.stripe.model.Subscription;
import com.stripe.param.CustomerCreateParams;
import com.stripe.param.CustomerListParams;
import com.stripe.param.CustomerUpdateParams;
import com.stripe.param.PaymentMethodAttachParams;
import com.stripe.param.SubscriptionCreateParams;

@Service
public class SubscriptionService {
	
	@Value("${stripe.key.secret}")
    String secretKey;
	
	public String subscribe(SubscriptionDto subscriptionDTO) throws StripeException {
		Stripe.apiKey = secretKey;
		Customer customer = null;
    	CustomerListParams customerListParams = CustomerListParams.builder()
    			.setEmail(subscriptionDTO.getEmail())
    			.build();
    	List<Customer> customerCollection = Customer.list(customerListParams).getData();
    	
    	if(customerCollection.size() == 1) {
    		customer = customerCollection.get(0);
    	} else {
    		CustomerCreateParams createParams = CustomerCreateParams.builder()
    				.setEmail(subscriptionDTO.getEmail())
    				.setName(subscriptionDTO.getName())
    				.setPaymentMethod(subscriptionDTO.getPaymentMethod())
    				.build();
    		customer = Customer.create(createParams);
    	}
    	
    	PaymentMethodAttachParams attachParams = PaymentMethodAttachParams.builder()
    			.setCustomer(customer.getId())
    			.build();
    	
    	PaymentMethod pm = PaymentMethod.retrieve(subscriptionDTO.getPaymentMethod());
    	pm = pm.attach(attachParams);
    	
    	String defaultPM = customer.getInvoiceSettings().getDefaultPaymentMethod();
    	if (defaultPM == null) {
    		CustomerUpdateParams updateParams = CustomerUpdateParams.builder()
    				.setInvoiceSettings(
    						CustomerUpdateParams.InvoiceSettings.builder()
    						
    						.setDefaultPaymentMethod(subscriptionDTO.getPaymentMethod())
    						.build()
    					).build();
    		customer = customer.update(updateParams);
    	}
    	
    	SubscriptionCreateParams subCreateParams = SubscriptionCreateParams
    		      .builder()
    		      .setCustomer(customer.getId())
    		      .addItem(
    		        SubscriptionCreateParams
    		          .Item.builder()
    		          .setPlan(subscriptionDTO.getPrice())
    		          .build()
    		      )
    		      .setPaymentBehavior(SubscriptionCreateParams.PaymentBehavior.ALLOW_INCOMPLETE)
    		      .addAllExpand(Arrays.asList("latest_invoice.payment_intent"))
    		      .build();

	    Subscription subscription = Subscription.create(subCreateParams);
	    HashMap<String, Object> responseData = new HashMap<>();
	    responseData.put("subscriptionId", subscription.getId());
	    responseData.put("clientSecret", subscription.getLatestInvoiceObject().getPaymentIntentObject().getClientSecret());
	    
	    return StripeObject.PRETTY_PRINT_GSON.toJson(responseData);
	}
}
