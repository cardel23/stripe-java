export interface PaymentIntentDto {
    token: string;
    description: string;
    amount: number;
    currency: string;
}

export interface SubscriptionDto {
    name: string;
    email: string;
    price: string;
    product: string;
    paymentMethod: string;
}


