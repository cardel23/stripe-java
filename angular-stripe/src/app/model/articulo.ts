export interface Articulo {
    id?: string;
    nombre: string;
    descripcion: string;
    precio: number;
    imagenURL: string;
    tipo?: string;
    plan?: string;
}

