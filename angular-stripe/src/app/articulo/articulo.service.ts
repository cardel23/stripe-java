import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Articulo } from '../model/articulo';

const cabecera = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

@Injectable({
  providedIn: 'root'
})
export class ArticuloService {
  articuloURL = 'http://localhost:8080/articulo/';

  articulos: Articulo[] = [{
    nombre: "Learn microservices with Spring Boot - 2nd Edition",
    descripcion: "A Practical Approach to RESTful Services Using an Event-Driven Architecture, Cloud-Native Patterns, and Containerization",
    id: "1",
    precio: 1000,
    imagenURL: "https://avatars.githubusercontent.com/u/63994591?s=200&v=4"
  },
  {
    nombre: "Basic subscription",
    descripcion: "Monthly subscription",
    id: 'prod_L2qDu8TVywZBvk',
    precio: 200,
    imagenURL: "",
    plan: "price_1KMkXMJr5Y1uPUlUJizxM6y1"
  },
  {
    nombre: "Advanced subscripion",
    descripcion: "Monthly subscription",
    id: "prod_L2qDyGG7bOV3BE",
    precio: 400,
    imagenURL: "",
    plan: 'price_1KMkX4Jr5Y1uPUlUVfOjA1js'
  },
  {
    nombre: "Full subscription",
    descripcion: "Monthly subscription",
    id: "prod_L2qDu8TVywZBvk",
    precio: 600,
    imagenURL: "",
    plan: "price_1KMkXMJr5Y1uPUlUJizxM6y1"
  },
];

  constructor(private httpClient: HttpClient) { }

  public lista(): Observable<Articulo[]> {
    return this.httpClient.get<Articulo[]>(this.articuloURL + 'lista', cabecera);
  }

  public detalle(id: number): Observable<Articulo> {
    return this.httpClient.get<Articulo>(this.articuloURL + `detalle/${id}`, cabecera);
  }

  public listaArticulos(): Articulo[] {
    return this.articulos;
  }

  public detalleArticulo(id: string): Articulo {
    const articulo = this.articulos.find(item=> item.id === id);
    return articulo;
  }


}
