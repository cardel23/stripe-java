import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PaymentIntentDto, SubscriptionDto } from '../model/payment-intent-dto';

const cabecera = {headers: new HttpHeaders({'Content-Type': 'application/json'})};

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  stripeURL = 'http://localhost:8080/api/stripe/';

  constructor(private httpClient: HttpClient) {}

  public pagar(paymentIntentDto: PaymentIntentDto): Observable<string> {
    return this.httpClient.post<string>(this.stripeURL + 'paymentIntent', paymentIntentDto, cabecera);
  }

  public confirmar(id: string): Observable<string> {
    return this.httpClient.post<string>(this.stripeURL + `confirmPayment/${id}`, {}, cabecera);
  }

  public cancelar(id: string): Observable<string> {
    return this.httpClient.post<string>(this.stripeURL + `cancelPayment/${id}`, {}, cabecera);
  }

  public subscribir(subscriptionDto: SubscriptionDto): Observable<string> {
    return this.httpClient.post<string>(this.stripeURL + `subscription/`, subscriptionDto, cabecera)
  }

}
