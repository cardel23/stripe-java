import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../modal/modal.component';
import { StripeService } from 'ngx-stripe';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PaymentService } from './payment.service';
import { Router } from '@angular/router';
import { PaymentIntentDto, SubscriptionDto } from '../model/payment-intent-dto';
import { StripeCardElement, StripeElements, StripeElementsOptions } from '@stripe/stripe-js';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  @Input() precio;
  @Input() descripcion;
  @Input() nombre;
  @Input() plan;
  @Input() id;

  error: any;

  elements: StripeElements;
  card: StripeCardElement;

  elementsOptions: StripeElementsOptions = {
    locale: 'es'
  };

  constructor(
    public modalService: NgbModal,
    private stripeService: StripeService,
    private paymentService: PaymentService,
    private toastrService: ToastrService,
    private router: Router
  ) { }

  public stripeForm = new FormGroup({
    name: new FormControl('', Validators.required)
  });

  ngOnInit() {
    this.stripeService.elements(this.elementsOptions)
      .subscribe(elements => {
        this.elements = elements;
        // Only mount the element the first time
        if (!this.card) {
          this.card = this.elements.create('card', {
            style: {
              base: {
                iconColor: '#666EE8',
                color: '#31325F',
                lineHeight: '40px',
                fontWeight: 300,
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '18px',
                '::placeholder': {
                  color: '#CFD7E0'
                }
              }
            }
          });
          this.card.mount('#card-element');
        }
      });
  }

  buy() {
    const name = this.stripeForm.get('name').value;

    this.stripeService
      .createPaymentMethod({
        type:"card",
        card: this.card,
        billing_details: {
          name: name,
          email: "cardel91@gmail.com"
        }
      })
      .subscribe(result => {
        if (result.paymentMethod) {
          if (this.plan) {
            const subscriptionDto: SubscriptionDto = {
              name: name,
              email: "cardel91@gmail.com",
              price: this.plan,
              product: this.id,
              paymentMethod: result.paymentMethod.id
            };

            this.paymentService.subscribir(subscriptionDto).subscribe(
              data => {
                console.log(data);
                this.toastrService.success
                ('subscription', 'subscriptionId'+data['subscriptionId'] + '\nclientSecret' +data[`clientSecret`], {positionClass: 'toast-top-center', timeOut: 0});
                this.router.navigate(['/']);
              }, err => {
                this.toastrService.error
                ('subscription', err, {positionClass: 'toast-top-center', timeOut: 0});
                console.log(err);
              }
            )

          } else {
            const paymentIntentDto: PaymentIntentDto = {
              token: result.paymentMethod.id,
              amount: this.precio,
              currency: 'eur',
              description: this.descripcion
            };
            this.paymentService.pagar(paymentIntentDto).subscribe(
              data => {
                console.log(data);
                this.abrirModal(data[`id`], this.nombre, data[`description`], data[`amount`]);
                this.router.navigate(['/']);
              }
            );
            this.error = undefined;
          }
        } else if (result.error) {
          this.error = result.error.message;
          this.toastrService.error
                ('error', this.error, {positionClass: 'toast-top-center', timeOut: 0});
          console.log(this.error);
        }
      });
  }

  abrirModal(id: string, nombre: string, descripcion: string, precio: number) {
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.nombre = nombre;
    modalRef.componentInstance.descripcion = descripcion;
    modalRef.componentInstance.precio = precio;
  }

}
